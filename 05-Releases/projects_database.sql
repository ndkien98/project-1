-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: projects_database2
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `category_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_code` (`category_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'TL01','Ứng dụng desktop'),(2,'TL02','Ứng dụng web'),(3,'TL03','Ứng dụng mobile');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `subject_group` int NOT NULL,
  `class_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `year_semester_id` int DEFAULT NULL,
  `lecturer_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `last_modified_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses` (`subject_code`,`subject_group`,`class_code`,`year_semester_id`),
  KEY `subject_code` (`subject_code`),
  KEY `lecturer_code` (`lecturer_code`),
  KEY `year_semester_id` (`year_semester_id`),
  CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`subject_code`) REFERENCES `subjects` (`subject_code`),
  CONSTRAINT `courses_ibfk_2` FOREIGN KEY (`lecturer_code`) REFERENCES `lecturers` (`lecturer_code`),
  CONSTRAINT `courses_ibfk_3` FOREIGN KEY (`year_semester_id`) REFERENCES `years_semesters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (9,'mh000003',1,'k61',4,'gv03','2020-09-08 09:14:22',NULL,'2020-09-08 09:18:46'),(10,'mh000003',1,'K62',4,'gv02','2020-09-08 09:14:41',NULL,'2020-09-08 09:14:41'),(11,'mh000003',2,'k53',4,'gv01','2020-09-08 09:14:56',NULL,'2020-09-08 09:14:56'),(12,'mh000002',1,'k61',1,'gv02','2020-09-13 11:35:23',NULL,'2020-09-13 11:35:23');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `department_code` varchar(5) COLLATE utf8mb4_general_ci NOT NULL,
  `department_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `department_code` (`department_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'TH01','Khoa học máy tính'),(2,'TH02','Công nghệ phần mềm'),(3,'TH03','Toán - Tin ứng dụng');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functions`
--

DROP TABLE IF EXISTS `functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `functions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `function_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `action_code` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `function_description` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action_code` (`action_code`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functions`
--

LOCK TABLES `functions` WRITE;
/*!40000 ALTER TABLE `functions` DISABLE KEYS */;
INSERT INTO `functions` VALUES (1,'Quản lý phân quyền','ROLE_MANAGEMENT',NULL),(2,'Thêm quyền','ADD_ROLE',NULL),(3,'Sửa quyền','EDIT_ROLE',NULL),(4,'Xoá quyền','DELETE_ROLE',NULL),(5,'Quản lý bộ môn','DEPARTMENT_MANAGEMENT',NULL),(6,'Thêm bộ môn','ADD_DEPARTMENT',NULL),(7,'Sửa bộ môn','EDIT_DEPARTMENT',NULL),(8,'Xoá bộ môn','DELETE_DEPARTMENT',NULL),(9,'Quản lý người dùng','USER_MANAGEMENT',NULL),(10,'Thêm giảng viên','ADD_LECTURER',NULL),(11,'Sửa giảng viên','EDIT_LECTURER',NULL),(12,'Xoá giảng viên','DELETE_LECTURER',NULL),(13,'Thêm sinh viên','ADD_STUDENT',NULL),(14,'Sửa sinh viên','EDIT_STUDENT',NULL),(15,'Xoá sinh viên','DELETE_STUDENT',NULL),(16,'Quản lý môn học','SUBJECT_MANAGEMENT',NULL),(17,'Thêm môn học','ADD_SUBJECT',NULL),(18,'Sửa môn học','EDIT_SUBJECT',NULL),(19,'Xoá môn học','DELETE_SUBJECT',NULL),(20,'Quản lý năm học - học kỳ','YEAR_SEMESTER_MANAGEMENT',NULL),(21,'Thêm năm học - học kỳ','ADD_YEAR_SEMSETER',NULL),(22,'Sửa năm học - học kỳ','EDIT_YEAR_SEMSETER',NULL),(23,'Xoá năm học - học kỳ','DELETE_YEAR_SEMSETER',NULL),(24,'Quản lý lớp học phần','COURSE_MANAGEMENT',NULL),(25,'Thêm lớp học phần','ADD_COURSE',NULL),(26,'Sửa lớp học phần','EDIT_COURSE',NULL),(27,'Xoá lớp học phần','DELETE_COURSE',NULL),(28,'Quản lý thể loại','CATEGORY_MANAGEMENT',NULL),(29,'Thêm thể loại','ADD_CATEGORY',NULL),(30,'Sửa thể loại','EDIT_CATEGORY',NULL),(31,'Xoá thể loại','DELETE_CATEGORY',NULL),(32,'Quản lý đồ án','PROJECT_MANAGEMENT',NULL),(33,'Thêm đồ án','ADD_PROJECT',NULL),(34,'Sửa đồ án','EDIT_PROJECT',NULL),(35,'Xoá đồ án','DELETE_PROJECT',NULL),(36,'Kiểm duyệt đồ án','PROJECT_APPROVE',NULL),(37,'Thống kê đồ án','PROJECT_ANALYZE',NULL);
/*!40000 ALTER TABLE `functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturers`
--

DROP TABLE IF EXISTS `lecturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lecturers` (
  `lecturer_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `department_code` varchar(5) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`lecturer_code`),
  KEY `department_code` (`department_code`),
  KEY `lecturer_code` (`lecturer_code`),
  CONSTRAINT `lecturers_ibfk_1` FOREIGN KEY (`lecturer_code`) REFERENCES `users` (`username`),
  CONSTRAINT `lecturers_ibfk_2` FOREIGN KEY (`department_code`) REFERENCES `departments` (`department_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturers`
--

LOCK TABLES `lecturers` WRITE;
/*!40000 ALTER TABLE `lecturers` DISABLE KEYS */;
INSERT INTO `lecturers` VALUES ('gv01','TH01'),('admin','TH02'),('gv03','TH02'),('gv02','TH03');
/*!40000 ALTER TABLE `lecturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_members`
--

DROP TABLE IF EXISTS `project_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `class_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `project_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_code` (`project_code`),
  CONSTRAINT `project_members_ibfk_1` FOREIGN KEY (`project_code`) REFERENCES `projects` (`project_code`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_members`
--

LOCK TABLES `project_members` WRITE;
/*!40000 ALTER TABLE `project_members` DISABLE KEYS */;
INSERT INTO `project_members` VALUES (29,'aaaa','bbbbb','ccccccccc','02'),(38,'aaaaaaa','aaaaaaaaa','aaaaaaaa','1'),(42,'aaaa','bbbbb','K61CNTTP','2'),(43,'aaaaaaaa','aaaaaaaaaa','aaaaaaaa','30'),(44,'a','a','a','01');
/*!40000 ALTER TABLE `project_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `project_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `project_avatar_url` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_general_ci,
  `detailed_description` text COLLATE utf8mb4_general_ci,
  `demo_link` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `category_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `student_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `last_modified_by` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `last_modified_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `project_code` (`project_code`),
  KEY `category_code` (`category_code`),
  KEY `student_code` (`student_code`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`category_code`) REFERENCES `categories` (`category_code`),
  CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`student_code`) REFERENCES `students` (`student_code`),
  CONSTRAINT `projects_ibfk_3` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (21,'02','Project 2','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/18c50aa7bc54e58fe2733807b80e15e1.jpg','Adobe Illustrator tutorial is the training series for the students and working professionals to learn Adobe graphics and publishing software developed with the support of Adobe product experts.','<h1 class=\"h1\" style=\"line-height: 1.3em; margin-top: 5px; font-family: erdana, helvetica, arial, sans-serif; font-size: 29px; color: rgb(97, 11, 56);\">Adobe Illustrator Tutorial</h1><p><img src=\"https://static.javatpoint.com/tutorial/adobe-illustrator/images/adobe-illustrator.png\" class=\"imageright\" alt=\"Adobe Illustrator Tutorial\" style=\"max-width: 100%; height: auto; margin-left: 20px; float: right; font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"></p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Adobe Illustrator tutorial is the training series for the students and working professionals to learn Adobe graphics and publishing software developed with the support of Adobe product experts.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">The tutorial is designed in a way that you can learn at your own pace. It will explain to you all about the Illustrator from fundamental to advance features, including the tips and techniques for using the latest version of the application.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">What is Adobe Illustrator</h3><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Adobe Illustrator is an application software service provided by Adobe. This software application can be used to create artwork using a&nbsp;<a href=\"https://www.javatpoint.com/windows\" style=\"color: green; text-decoration: none;\">Windows</a>&nbsp;PC or MacOS computer. The first version of this application was released in 1987 and continuously evolving day by day. Now, it will be available to be used on the cloud, i.e., Adobe Creative Cloud. To create high-quality artwork, graphic designers, web designers, visual artists, and professional illustrators use this application. It includes a vast number of drawing tools that we can use to reduce the time need to create an illustration.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">These are the following things that you can do with illustrator.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"><strong style=\"color: rgba(0, 0, 0, 0.75);\">Example:</strong></p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Create a variety of digital and printed images.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Cartoons.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Charts.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Diagram.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Graphs.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Logos.</li></ul><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">We can use an image as a guide to trace an object in the photograph and can also import that image. It can be used to create a sketch-like appearance and re-coloring of the picture. The illustrator can be used to manipulate text in many ways and can be used to create postcards, posters, and other visual designs that always uses image and writing together.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">Adobe Illustrator CC</h3><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Adobe Illustrator CC is the cloud version of the same that stands for Adobe creative cloud. It is Adobe\'s cloud-based subscription service, which was first released in 2013.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">An important change in Adobe Illustrator CC is that it includes the ability to save the document over the cloud. Its data can be synced simultaneously to the cloud to keep our setting updated. Adobe is now integrated with Behance, which can be used by the artists to showcase their work and portfolios.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">History</h3><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">The first version of the illustrator was released in 1987 for the Mac OS. Adobe was always focused on developing fonts known as PostScript so that computers could use to communicate with printers.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">The 2<sup>nd</sup>&nbsp;version of the illustrator becomes the first version of the illustrator that can be used on&nbsp;<a href=\"https://www.javatpoint.com/what-is-windows\" style=\"color: green; text-decoration: none;\">Windows OS</a>, and it was released in later 1989. After that, it will be developed to use on NeXT, Silicon Graphics, and Sun Solaris in the 1990s.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">The latest version of the Illustrator in CS6 and became part of the Creative Cloud. The current version is Adobe Illustrator CC 2020.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Prerequisites</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Before learning Adobe Illustrator CC, you should have a working knowledge of the computer and its operating system. Make sure you know how to use the basic commands and menus with primary I/O devices.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"><strong style=\"color: rgba(0, 0, 0, 0.75);\">In this adobe illustrator tutorial, we are going to discuss the following topics -</strong></p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Installation of Illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Work Area of Illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Drawing in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Color in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Color Management in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Painting in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Selecting and arranging objects in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Reshaping objects in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Importing, exporting and saving in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Illustrator type</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Special Effects</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Web graphics</li></ul><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Audience</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Our Adobe Illustrator Tutorial is designed to help beginners and professionals.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Problem</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">We assure you that you will not find any discrepancies or problems in this tutorial. But in case, if you find any mistake, you can inform us by posting it in the contact form.</p>','https://www.javatpoint.com/adobe-illustrator','TL02','sv1',9,1,'2020-09-12 17:02:49','gv03',NULL,'2020-09-12 19:23:39'),(22,'1','project 03','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/15555973_1102945323184936_2132821312_n.jpg','Adobe Illustrator is an application software service provided by Adobe. This software application can be used to create artwork using a Windows PC or MacOS computer. The first version of this application was released in 1987 and continuously evolving day by day. Now, it will be available to be used on the cloud, i.e., Adobe Creative Cloud. To create high-quality artwork, graphic designers, web designers, visual artists, and professional illustrators use this application. It includes a vast number of drawing tools that we can use to reduce the time need to create an illustration.','<table style=\"width: 878px; font-family: Verdana, Arial, sans-serif; font-size: 13px;\"><tbody><tr><td style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: 23px; font-family: verdana, helvetica, arial, sans-serif; margin-left: 20px;\"><div id=\"bottomnextup\"><a class=\"next\" href=\"https://www.javatpoint.com/adobe-illustrator-installation\" style=\"color: rgb(255, 255, 255); text-decoration: none; font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: 700; font-stretch: normal; line-height: 1.5; font-family: &quot;times new roman&quot;; padding: 6px 25px; background-color: rgb(139, 195, 74); float: right; margin-left: 7px;\">next →</a></div><h1 class=\"h1\" style=\"line-height: 1.3em; margin-top: 5px; font-family: erdana, helvetica, arial, sans-serif; font-size: 29px; color: rgb(97, 11, 56);\">Adobe Illustrator Tutorial</h1><img src=\"https://static.javatpoint.com/tutorial/adobe-illustrator/images/adobe-illustrator.png\" class=\"imageright\" alt=\"Adobe Illustrator Tutorial\" style=\"max-width: 100%; height: auto; margin-left: 20px; float: right;\"><p>Adobe Illustrator tutorial is the training series for the students and working professionals to learn Adobe graphics and publishing software developed with the support of Adobe product experts.</p><p>The tutorial is designed in a way that you can learn at your own pace. It will explain to you all about the Illustrator from fundamental to advance features, including the tips and techniques for using the latest version of the application.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">What is Adobe Illustrator</h3><p>Adobe Illustrator is an application software service provided by Adobe. This software application can be used to create artwork using a&nbsp;<a href=\"https://www.javatpoint.com/windows\" style=\"color: green; text-decoration: none;\">Windows</a>&nbsp;PC or MacOS computer. The first version of this application was released in 1987 and continuously evolving day by day. Now, it will be available to be used on the cloud, i.e., Adobe Creative Cloud. To create high-quality artwork, graphic designers, web designers, visual artists, and professional illustrators use this application. It includes a vast number of drawing tools that we can use to reduce the time need to create an illustration.</p><p>These are the following things that you can do with illustrator.</p><p><strong style=\"color: rgba(0, 0, 0, 0.75);\">Example:</strong></p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Create a variety of digital and printed images.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Cartoons.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Charts.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Diagram.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Graphs.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Logos.</li></ul><p>We can use an image as a guide to trace an object in the photograph and can also import that image. It can be used to create a sketch-like appearance and re-coloring of the picture. The illustrator can be used to manipulate text in many ways and can be used to create postcards, posters, and other visual designs that always uses image and writing together.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">Adobe Illustrator CC</h3><p>Adobe Illustrator CC is the cloud version of the same that stands for Adobe creative cloud. It is Adobe\'s cloud-based subscription service, which was first released in 2013.</p><p>An important change in Adobe Illustrator CC is that it includes the ability to save the document over the cloud. Its data can be synced simultaneously to the cloud to keep our setting updated. Adobe is now integrated with Behance, which can be used by the artists to showcase their work and portfolios.</p><h3 class=\"h3\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 75); font-size: 21px;\">History</h3><p>The first version of the illustrator was released in 1987 for the Mac OS. Adobe was always focused on developing fonts known as PostScript so that computers could use to communicate with printers.</p><p>The 2<sup>nd</sup>&nbsp;version of the illustrator becomes the first version of the illustrator that can be used on&nbsp;<a href=\"https://www.javatpoint.com/what-is-windows\" style=\"color: green; text-decoration: none;\">Windows OS</a>, and it was released in later 1989. After that, it will be developed to use on NeXT, Silicon Graphics, and Sun Solaris in the 1990s.</p><p>The latest version of the Illustrator in CS6 and became part of the Creative Cloud. The current version is Adobe Illustrator CC 2020.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Prerequisites</h2><p>Before learning Adobe Illustrator CC, you should have a working knowledge of the computer and its operating system. Make sure you know how to use the basic commands and menus with primary I/O devices.</p><p><strong style=\"color: rgba(0, 0, 0, 0.75);\">In this adobe illustrator tutorial, we are going to discuss the following topics -</strong></p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Installation of Illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Work Area of Illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Drawing in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Color in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Color Management in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Painting in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Selecting and arranging objects in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Reshaping objects in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Importing, exporting and saving in illustrator</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Illustrator type</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Special Effects</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Web graphics</li></ul><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Audience</h2><p>Our Adobe Illustrator Tutorial is designed to help beginners and professionals.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Problem</h2><p>We assure you that you will not find any discrepancies or problems in this tutorial. But in case, if you find any mistake, you can inform us by posting it in the contact form.</p></td></tr></tbody></table>','http://localhost:4201/management/project/add','TL02','sv1',10,0,'2020-09-12 22:22:54','gv03',NULL,'2020-09-12 22:27:46'),(23,'2','project 03','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/18922196_814209408746471_7780765731965042722_n.jpg','a','<div id=\"main-content\" jstcache=\"0\" style=\"color: rgb(95, 99, 104); font-family: sans, Arial, sans-serif; font-size: 15px;\"><div id=\"main-message\" jstcache=\"0\"><h1 jstcache=\"0\" style=\"font-size: 1.6em; line-height: 1.25em; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; overflow-wrap: break-word;\"><span jsselect=\"heading\" jsvalues=\".innerHTML:msg\" jstcache=\"10\">This site can’t be reached</span></h1><p jsselect=\"summary\" jsvalues=\".innerHTML:msg\" jstcache=\"2\" style=\"display: inline;\">The webpage at&nbsp;<strong jscontent=\"failedUrl\" jstcache=\"23\" style=\"overflow-wrap: break-word;\">https://www.javatpoint.com/select-and-arrange-objects-in-adobe-illustrator</strong>&nbsp;might be temporarily down or it may have moved permanently to a new web address.</p><div id=\"error-information-popup-container\" jstcache=\"0\"><div id=\"error-information-popup\" jstcache=\"0\"><div id=\"error-information-popup-box\" jstcache=\"0\"><div id=\"error-information-popup-content\" jstcache=\"0\"><div class=\"error-code\" jscontent=\"errorCode\" jstcache=\"18\" style=\"color: var(--error-code-color); font-size: 0.8em; text-transform: uppercase; margin-top: 12px;\">ERR_FAILED</div><div><br></div></div></div></div></div></div></div>','http://localhost:4201/management/project/add','TL01','sv1',11,0,'2020-09-12 22:26:39','gv03',NULL,'2020-09-15 15:56:29'),(24,'04','project 04','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/18c50aa7bc54e58fe2733807b80e15e1.jpg','\nnext →\nLearn JavaScript Tutorial\nJavaScript Tutorial\nOur JavaScript Tutorial is designed for beginners and professionals both. JavaScript is used to create client-side dynamic pages.\n\nJavaScript is an object-based scripting language which is lightweight and cross-platform.\n\nJavaScript is not a compiled language, but it is a translated language. The JavaScript Translator (embedded in the browser) is responsible for translating the JavaScript code for the web browser.','<h1 class=\"h1\" style=\"line-height: 1.3em; margin-top: 5px; font-family: erdana, helvetica, arial, sans-serif; font-size: 29px; color: rgb(97, 11, 56);\">Learn JavaScript Tutorial</h1><p><img src=\"https://static.javatpoint.com/images/javascript/javascript_logo.png\" width=\"200\" height=\"200\" alt=\"JavaScript Tutorial\" class=\"imageright\" style=\"max-width: 100%; height: auto; margin-left: 20px; float: right; font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"></p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Our&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">JavaScript Tutorial</strong>&nbsp;is designed for beginners and professionals both. JavaScript is used to create client-side dynamic pages.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is&nbsp;<em>an object-based scripting language</em>&nbsp;which is lightweight and cross-platform.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is not a compiled language, but it is a translated language. The JavaScript Translator (embedded in the browser) is responsible for translating the JavaScript code for the web browser.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">What is JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript (js) is a light-weight object-oriented programming language which is used by several websites for scripting the webpages. It is an interpreted, full-fledged programming language that enables dynamic interactivity on websites when applied to an HTML document. It was introduced in the year 1995 for adding programs to the webpages in the Netscape Navigator browser. Since then, it has been adopted by all other graphical web browsers. With JavaScript, users can build modern web applications to interact directly without reloading the page every time. The traditional website uses js to provide several forms of interactivity and simplicity.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Although, JavaScript has no connectivity with Java programming language. The name was suggested and provided in the times when Java was gaining popularity in the market. In addition to web browsers, databases such as CouchDB and MongoDB uses JavaScript as their scripting and query language.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Features of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">There are following features of JavaScript:</p><ol class=\"points\" style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">All popular web browsers support JavaScript as they provide built-in execution environments.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript follows the syntax and structure of the C programming language. Thus, it is a structured programming language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is a weakly typed language, where certain types are implicitly cast (depending on the operation).</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is an object-oriented programming language that uses prototypes rather than using classes for inheritance.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It is a light-weighted and interpreted language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It is a case-sensitive language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is supportable in several operating systems including, Windows, macOS, etc.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It provides good control to the users over the web browsers.</li></ol><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">History of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">In 1993,&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Mosaic</strong>, the first popular web browser, came into existence. In the&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">year 1994</strong>,&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Netscape</strong>&nbsp;was founded by&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Marc Andreessen</strong>. He realized that the web needed to become more dynamic. Thus, a \'glue language\' was believed to be provided to HTML to make web designing easy for designers and part-time programmers. Consequently, in 1995, the company recruited&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Brendan Eich</strong>&nbsp;intending to implement and embed Scheme programming language to the browser. But, before Brendan could start, the company merged with&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Sun Microsystems</strong>&nbsp;for adding Java into its Navigator so that it could compete with Microsoft over the web technologies and platforms. Now, two languages were there: Java and the scripting language. Further, Netscape decided to give a similar name to the scripting language as Java\'s. It led to \'Javascript\'. Finally, in May 1995, Marc Andreessen coined the first code of Javascript named \'<strong style=\"color: rgba(0, 0, 0, 0.75);\">Mocha</strong>\'. Later, the marketing team replaced the name with \'<strong style=\"color: rgba(0, 0, 0, 0.75);\">LiveScript</strong>\'. But, due to trademark reasons and certain other reasons, in December 1995, the language was finally renamed to \'JavaScript\'. From then, JavaScript came into existence.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Application of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is used to create interactive websites. It is mainly used for:</p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Client-side validation,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Dynamic drop-down menus,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying date and time,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying pop-up windows and dialog boxes (like an alert dialog box, confirm dialog box and prompt dialog box),</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying clocks etc.</li></ul><h3 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">JavaScript Example</h3>','https://www.javatpoint.com/javascript-tutorial','TL03','sv1',12,1,'2020-09-13 11:46:32','gv03',NULL,'2020-09-15 15:56:33'),(37,'20','project 20','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/18c50aa7bc54e58fe2733807b80e15e1.jpg','a','<p>a</p>','a','TL02','sv1',9,1,'2020-09-13 11:59:28','gv03',NULL,'2020-09-15 15:56:36'),(40,'30','project 30','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/Cristiano-Ronaldo-Desktop-Wallpaper-.jpg','a','<h1 class=\"h1\" style=\"line-height: 1.3em; margin-top: 5px; font-family: erdana, helvetica, arial, sans-serif; font-size: 29px; color: rgb(97, 11, 56);\">Learn JavaScript Tutorial</h1><p><img src=\"https://static.javatpoint.com/images/javascript/javascript_logo.png\" width=\"200\" height=\"200\" alt=\"JavaScript Tutorial\" class=\"imageright\" style=\"max-width: 100%; height: auto; margin-left: 20px; float: right; font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"></p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Our&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">JavaScript Tutorial</strong>&nbsp;is designed for beginners and professionals both. JavaScript is used to create client-side dynamic pages.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is&nbsp;<em>an object-based scripting language</em>&nbsp;which is lightweight and cross-platform.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is not a compiled language, but it is a translated language. The JavaScript Translator (embedded in the browser) is responsible for translating the JavaScript code for the web browser.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">What is JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript (js) is a light-weight object-oriented programming language which is used by several websites for scripting the webpages. It is an interpreted, full-fledged programming language that enables dynamic interactivity on websites when applied to an HTML document. It was introduced in the year 1995 for adding programs to the webpages in the Netscape Navigator browser. Since then, it has been adopted by all other graphical web browsers. With JavaScript, users can build modern web applications to interact directly without reloading the page every time. The traditional website uses js to provide several forms of interactivity and simplicity.</p><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">Although, JavaScript has no connectivity with Java programming language. The name was suggested and provided in the times when Java was gaining popularity in the market. In addition to web browsers, databases such as CouchDB and MongoDB uses JavaScript as their scripting and query language.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Features of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">There are following features of JavaScript:</p><ol class=\"points\" style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">All popular web browsers support JavaScript as they provide built-in execution environments.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript follows the syntax and structure of the C programming language. Thus, it is a structured programming language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is a weakly typed language, where certain types are implicitly cast (depending on the operation).</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is an object-oriented programming language that uses prototypes rather than using classes for inheritance.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It is a light-weighted and interpreted language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It is a case-sensitive language.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">JavaScript is supportable in several operating systems including, Windows, macOS, etc.</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">It provides good control to the users over the web browsers.</li></ol><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">History of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">In 1993,&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Mosaic</strong>, the first popular web browser, came into existence. In the&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">year 1994</strong>,&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Netscape</strong>&nbsp;was founded by&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Marc Andreessen</strong>. He realized that the web needed to become more dynamic. Thus, a \'glue language\' was believed to be provided to HTML to make web designing easy for designers and part-time programmers. Consequently, in 1995, the company recruited&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Brendan Eich</strong>&nbsp;intending to implement and embed Scheme programming language to the browser. But, before Brendan could start, the company merged with&nbsp;<strong style=\"color: rgba(0, 0, 0, 0.75);\">Sun Microsystems</strong>&nbsp;for adding Java into its Navigator so that it could compete with Microsoft over the web technologies and platforms. Now, two languages were there: Java and the scripting language. Further, Netscape decided to give a similar name to the scripting language as Java\'s. It led to \'Javascript\'. Finally, in May 1995, Marc Andreessen coined the first code of Javascript named \'<strong style=\"color: rgba(0, 0, 0, 0.75);\">Mocha</strong>\'. Later, the marketing team replaced the name with \'<strong style=\"color: rgba(0, 0, 0, 0.75);\">LiveScript</strong>\'. But, due to trademark reasons and certain other reasons, in December 1995, the language was finally renamed to \'JavaScript\'. From then, JavaScript came into existence.</p><h2 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">Application of JavaScript</h2><p style=\"font-family: verdana, helvetica, arial, sans-serif; font-size: 13px;\">JavaScript is used to create interactive websites. It is mainly used for:</p><ul class=\"points\" style=\"list-style: circle; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: verdana, helvetica, arial, sans-serif;\"><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Client-side validation,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Dynamic drop-down menus,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying date and time,</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying pop-up windows and dialog boxes (like an alert dialog box, confirm dialog box and prompt dialog box),</li><li style=\"padding: 0.2em; line-height: 21px; margin-top: 4px;\">Displaying clocks etc.</li></ul><h3 class=\"h2\" style=\"line-height: 1.3em; font-family: erdana, helvetica, arial, sans-serif; color: rgb(97, 11, 56); font-size: 25px;\">JavaScript Example</h3>','https://www.javatpoint.com/javascript-tutorial','TL01','sv1',9,0,'2020-09-13 12:04:33','gv03',NULL,'2020-09-15 15:56:38'),(41,'01','final','/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/project/18c50aa7bc54e58fe2733807b80e15e1.jpg','a','<p>a</p>','a','TL02','sv1',10,1,'2020-09-15 15:53:32','gv03',NULL,'2020-09-15 15:56:41');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin'),(2,'Lecture'),(3,'Student');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_functions`
--

DROP TABLE IF EXISTS `roles_functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_functions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `function_id` int NOT NULL,
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id_function_id` (`role_id`,`function_id`),
  KEY `role_id` (`role_id`),
  KEY `function_id` (`function_id`),
  CONSTRAINT `roles_functions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `roles_functions_ibfk_2` FOREIGN KEY (`function_id`) REFERENCES `functions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_functions`
--

LOCK TABLES `roles_functions` WRITE;
/*!40000 ALTER TABLE `roles_functions` DISABLE KEYS */;
INSERT INTO `roles_functions` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,1),(10,1,10,1),(11,1,11,1),(12,1,12,1),(13,1,13,1),(14,1,14,1),(15,1,15,1),(16,1,16,1),(17,1,17,1),(18,1,18,1),(19,1,19,1),(20,1,20,1),(21,1,21,1),(22,1,22,1),(23,1,23,1),(24,1,24,1),(25,1,25,1),(26,1,26,1),(27,1,27,1),(28,1,28,1),(29,1,29,1),(30,1,30,1),(31,1,31,1),(32,1,32,1),(33,1,33,1),(34,1,34,1),(35,1,35,1),(36,1,36,1),(37,1,37,1),(38,2,1,0),(39,2,2,0),(40,2,3,0),(41,2,4,0),(42,2,5,0),(43,2,6,0),(44,2,7,0),(45,2,8,0),(47,2,10,0),(48,2,11,1),(49,2,12,0),(50,2,13,1),(51,2,14,1),(52,2,15,1),(53,2,16,0),(54,2,17,0),(55,2,18,0),(56,2,19,0),(57,2,20,0),(58,2,21,0),(59,2,22,0),(60,2,23,0),(61,2,24,1),(62,2,25,1),(63,2,26,1),(64,2,27,1),(65,2,28,0),(66,2,29,0),(67,2,30,0),(68,2,31,0),(69,2,32,1),(70,2,33,1),(71,2,34,1),(72,2,35,1),(73,2,36,1),(74,2,37,0),(75,3,1,0),(76,3,2,0),(77,3,3,0),(78,3,4,0),(79,3,5,0),(80,3,6,0),(81,3,7,0),(82,3,8,0),(83,3,9,1),(84,3,10,0),(85,3,11,0),(86,3,12,0),(87,3,13,0),(88,3,14,1),(89,3,15,0),(90,3,16,0),(91,3,17,0),(92,3,18,0),(93,3,19,0),(94,3,20,0),(95,3,21,0),(96,3,22,0),(97,3,23,0),(98,3,24,0),(99,3,25,0),(100,3,26,0),(101,3,27,0),(102,3,28,0),(103,3,29,0),(104,3,30,0),(105,3,31,0),(106,3,32,1),(107,3,33,1),(108,3,34,0),(109,3,35,0),(110,3,36,0),(111,3,37,0),(112,1,9,1);
/*!40000 ALTER TABLE `roles_functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `student_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `class_code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`student_code`),
  KEY `student_code` (`student_code`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`student_code`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES ('611293','CNPM'),('611294','K61CNTTP'),('sv','K61CNTTP'),('sv1','a'),('sv2','a');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subjects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `subject_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `department_code` varchar(5) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_code` (`subject_code`),
  KEY `department_code` (`department_code`),
  CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`department_code`) REFERENCES `departments` (`department_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (4,'mh000002','Giai tich','TH03'),(5,'mh000003','Phat trien ung dung web','TH02'),(6,'mh000001','Do an 2','TH02');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_message`
--

DROP TABLE IF EXISTS `system_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `system_message` (
  `id` int NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_message`
--

LOCK TABLES `system_message` WRITE;
/*!40000 ALTER TABLE `system_message` DISABLE KEYS */;
INSERT INTO `system_message` VALUES (1,'First Level Message'),(2,'Second Level Message'),(3,'Third Level Message');
/*!40000 ALTER TABLE `system_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email_address` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role_id` int NOT NULL,
  `user_avatar_url` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email_address` (`email_address`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (28,'611293','$2a$10$VDC.//UDny/eEAKs0dFhZOZ3aDoOk7OKvJv4p1QgTCQuowhcJfHXm','Nguyễn Tùng Bái','2020-09-09','Nam','tungbai@gmail.com','0925334772',3,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/stock-photo-1020869869.jpg',1,'2020-08-03 00:00:00','admin'),(29,'611294','$2a$10$gr.1OqCa7oNtduJGRorLV.ErZKWTrWNbUsR6gK6vqj09H0U.44VHi','Nguyễn Văn Dần','1998-09-16','Nam','tungba11111i@gmail.com','0925334772',3,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/Screenshot from 2020-09-03 00-12-33.png',1,'2020-08-03 00:00:00','admin'),(31,'sv','$2a$10$Q/RDFBMU0hwXtogZbtqHWO96bS1/yCgTl3gsyApULSnQd.OZjcEFa','Nguyen van sinh vien','2020-09-22','Nam','tungba1111111111111i@gmail.com','0925334772',3,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/desktop_wallpaper___cristiano_ronaldo_by_enihal_daz6b3o-fullview.jpg',1,'2020-08-04 00:00:00','gv03'),(34,'gv01','$2a$10$aYmNsYXSnzKY2V3Z.tHqquAl83XvmVR.zefUvCiDINswubzBOQtA.','Phạm Thị Lan Anh','1977-09-15','Nữ','ptlanh@gmail.com','0925334772',2,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/18738894_1949762525264523_7581062460017924162_o.jpg',1,'2020-08-08 00:00:00','admin'),(35,'gv02','$2a$10$GfYFR.wYE4H5B8HP/9MAU.u0bec59V1YIER/qp8gbCqJS8RFLkL2S','Nguyễn Văn Hoàng','1982-07-14','Nam','gv02@gmail.com','0925334772',2,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/b571997e430407f176a555579f5f435f.jpg',1,'2020-08-08 00:00:00','admin'),(36,'gv03','$2a$10$38wMmZJcqMDxNIofFz5eteRIRfs5T3.w7wDzR27GcEFpF/uQ9Aoii','Nguyễn Thị Huyền','1976-09-25','Nữ','gv03@gmail.com','0925334772',2,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/b571997e430407f176a555579f5f435f.jpg',1,'2020-08-08 00:00:00','admin'),(37,'admin','$2a$10$qc.kC.HVw2uLyH542rZtieFiqmanVIqV.rUk1SbHn70TCw60lQIfy','admin','2020-09-24','Nam','admin@gmail.com','0925334772',1,'/home/kiennd/group01-03project1/04-Source/Back-end/project/document/img/user/18738894_1949762525264523_7581062460017924162_o.jpg',1,'2020-08-08 00:00:00','gv03'),(41,'sv1','$2a$10$UUhKr10jm3TkyfoDF9xC6ODLYD.mdsg7SR24ljrzvd5X8yzNiunNC','Nguyen van sinh vien 1','2020-09-10','Nam','Nguyen van sinh vien@gmail.com','1',3,NULL,1,'2020-08-16 00:00:00','admin'),(42,'sv2','$2a$10$A0WUZ1XrquTJqW5oHhdDUO5x5vQ82z5uOuOmfjrakfe65dlGoqLL2','Nguyen van sinh vien2','2020-09-05','Nam','Nguyen van sinh vien2@gmail.com','1',3,NULL,1,'2020-08-16 00:00:00','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `years_semesters`
--

DROP TABLE IF EXISTS `years_semesters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `years_semesters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `year` int NOT NULL,
  `semester` int NOT NULL,
  `start_date` date DEFAULT NULL,
  `weeks_number` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `year` (`year`,`semester`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `years_semesters`
--

LOCK TABLES `years_semesters` WRITE;
/*!40000 ALTER TABLE `years_semesters` DISABLE KEYS */;
INSERT INTO `years_semesters` VALUES (1,2020,1,NULL,20),(2,2020,2,NULL,20),(4,2021,2,NULL,21);
/*!40000 ALTER TABLE `years_semesters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-20 23:47:08
