
export class YearsSemester {
  id: number;
  year: number;
  semester: number;
  startDate: any;
  weeksNumber: number;
 }
