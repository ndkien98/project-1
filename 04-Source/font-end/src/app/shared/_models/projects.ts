import {MembersProject} from "./members_project";

export class Projects {
  id;
  projectCode;
  projectName;
  projectAvatarUrl;
  shortDescription;
  detailedDescription;
  demoLink;
  categoryCode;
  categoryName;
  studentCode;
  studentName;
  studentClass;
  courseId;
  subjectCode;
  subjectName;
  subjectGroup;
  courseClass;
  yearSemesterId;
  year;
  semester;
  lecturerCode;
  lecturerName;
  status;
  createdDate;
  createdBy;
  lastModifiedBy;
  lastModifiedDate;
  projectMembers: MembersProject[];

}
