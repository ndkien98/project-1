import {HttpHeaders} from '@angular/common/http';

export const reload = true;

export const ERROR_INSERT = 0;

export const SUCCESS = 1;

export const ERROR_LOAD_DATA = 2;

export const baseUrl = 'http://localhost:8080/';

export const baseUrlLocal = 'http://localhost:8080/';

export const test = 'https://st-dse.vnua.edu.vn:6885/qly-do-an/';

export const ID_ROLE_STUDENT = 3;

export const ID_ROLE_LECTURE = 2;

export const ID_ROLE_ADMIN = 1;

export const PROJECT_ENABLE = 1;

export const PROJECT_DISABLE = 0;

export const DES_LOAD_DATA_FORM_SERVER = 'Lỗi tải dữ liệu từ máy chủ, đề nghị tải lại trang hoặc kiêm tra kết nối internet!';

export const routerHome = '/management/home';
export const routerDecentralization = '/management/decentralization';
export const routerDepartment = '/management/department';
export const routerYearsSemesters = '/management/years-semesters';
export const routerCategory = '/management/category';
export const routerCourses = '/management/courses';
export const routerSubjects = '/management/subjects';
export const routerStatistical = '/management/statistical';
export const routerProject = '/management/project';
export const routerProjectDetail = 'project/detail/';
export const routerProjectAdd  = '/management/project/add';
export const routerProjectEdit = 'project/edit/';
export const routerRevise = '/management/revise';
export const routerUser = '/management/user';



export const httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

export class DataConvertSelect2 {
  id: any;
  text: any;

  constructor(id?: any, text?: any) {
    this.id = id;
    this.text = text;
  }
}
