import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from 'rxjs';
import {AuthenticationService} from '../_service/authentication.service';


@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authentication: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userCurrent = this.authentication.getCurrentUser;
    if (userCurrent && userCurrent.token) {
      req = req.clone({
        setHeaders: {
          Authorization: `${userCurrent.token}`,
        }
      });
    }
    // let newRequest;
    // if (!userCurrent) {
    //   newRequest = req.clone({
    //     headers: req.headers,
    //     body: req.body,
    //   });
    // } else {
    //     // console.log('body', req.body);
    //     let headers = req.headers.set('Authorization', 'Bearer ' + userCurrent.token);
    //         headers = headers
    //           .append('Cache-Control', 'no-cache')
    //           .append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
    //           .append('Access-Control-Allow-Origin', '*')
    //           .append('Cache-Control', 'no-store')
    //           .append('Expires', '0');
    //           // .append('Pragma', 'no-cache');
    //         newRequest = req.clone({
    //           headers,
    //           body: req.body,
    //         });
    // }
    return next.handle(req);
  }
}
