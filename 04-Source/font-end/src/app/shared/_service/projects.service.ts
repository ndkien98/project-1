import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProjectsService {
  constructor(public http: HttpClient, private baseService: BaseService) {
  }

  // Http headers
  httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  getProjectQuantityBy(yearBegin, yearEnd): Observable<any> {
    const params = new HttpParams()
      .set('year', yearBegin)
      .set('year', yearEnd);
    // @ts-ignore
    // tslint:disable-next-line:ban-types max-line-length
    return this.http.post<any>(this.baseService.baseUrl + '/qly-do-an/api/projects/', {params})
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  // xử lý đưa ra lỗi nếu có
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
