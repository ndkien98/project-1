import {Injectable} from "@angular/core";
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {baseUrl, httpOptions} from "../_models/constant";
import {catchError, retry} from "rxjs/operators";
import {Projects} from "../_models/projects";


@Injectable({
  providedIn: 'root'
})
export class ProjectService extends BaseService {
  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getAllProject(): Observable<Projects[]> {
    return this.http.get<Projects[]>(baseUrl + 'api/projects/get-all')
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  getProjectByStatus(status): Observable<any> {
    return this.http.get<any>(baseUrl + 'api/projects/get-by-status/' + status)
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  getProjectById(id): Observable<any> {
    return this.http.get<any>(baseUrl + 'api/projects/get-by-id/' + id)
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  addProject(data): Observable<Projects> {
    return this.http.post<Projects>(baseUrl + 'api/projects/add', JSON.stringify(data), httpOptions)
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  editProject(data, id): Observable<any> {
    return this.http.put<any>(baseUrl + 'api/projects/edit/' + id, JSON.stringify(data), httpOptions)
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  findProjectById(id): Observable<Projects> {
    return this.http.get<Projects>(baseUrl + 'api/projects/get-by-id/' + id)
      .pipe(
        retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  getProjectByYearSemesterId(id): Observable<boolean> {
    // @ts-ignore
    return this.http.get<Projects[]>(baseUrl + 'api/projects/get-by-year-semester-id/' + id)
      .pipe(
        retry(1),
        catchError(BaseService.errorHandl)
      );

  }
  addAvatar(file: File, id): Observable<Projects> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('id', id);
    return this.http.put<Projects>(baseUrl + 'api/projects/edit/upload-avatar', formData)
      .pipe(
        retry(1),
        catchError(BaseService.errorHandl)
      );
  }

  // tslint:disable-next-line:ban-types
  deleteProject(id): Observable<Boolean> {
    // tslint:disable-next-line:ban-types
    return this.http.delete<Boolean>(baseUrl + 'api/projects/delete/' + id)
      .pipe(
        retry(1),
        catchError(BaseService.errorHandl)
      );
  }
}
