import {Injectable} from '@angular/core';
import {throwError} from 'rxjs';
import {DES_LOAD_DATA_FORM_SERVER} from "../_models/constant";
import {AuthenticationService} from "./authentication.service";


@Injectable({
  providedIn: 'root'
})
export class BaseService {
  constructor(
  ) {
  }

  static errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  // @ts-ignore
  getObjectById(idObj, listObj: any): any {
    let response: any;
    listObj.map(obj => {
      if (idObj == obj.id) {
        response = obj;
      }
    });
    return response;
  }
  static checkNullOrUndefine(obj: any) {
    if (!obj) {
      alert(DES_LOAD_DATA_FORM_SERVER);
      // window.location.reload();
    }
  }
}
