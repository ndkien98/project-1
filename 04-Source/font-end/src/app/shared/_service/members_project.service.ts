import {Injectable} from "@angular/core";
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Projects} from "../_models/projects";
import {baseUrl} from "../_models/constant";
import {catchError, retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MembersProjectService extends BaseService {
  constructor(
    private http: HttpClient
  ) {
    super();
  }

  getAllProject(): Observable<Projects[]> {
    return this.http.get<Projects[]>(baseUrl + 'api/projects/get-all')
      .pipe(retry(1),
        catchError(BaseService.errorHandl)
      );
  }
}
