import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Select2OptionData} from 'ng-select2';
import {
  DataConvertSelect2,
  DES_LOAD_DATA_FORM_SERVER,
  ERROR_INSERT,
  ERROR_LOAD_DATA, ID_ROLE_ADMIN, ID_ROLE_LECTURE, ID_ROLE_STUDENT,
  SUCCESS
} from '../../../shared/_models/constant';
import {Options} from 'select2';
import {RoleService} from '../../../shared/_service/role.service';
import {UserService} from '../../../shared/_service/user.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DetailUserComponent} from './detail-user/detail-user.component';
import {AddUserComponent} from './add-user/add-user.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {DeleteUserComponent} from './delete-user/delete-user.component';
import {User} from "../../../shared/_models/user";
import {AuthenticationService} from "../../../shared/_service/authentication.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  roles: any = [];      // chứa danh sách role
  users: any = [];        // chứa danh sách user từ api
  dataForTable: any = []; // chứa dữ liệu để đưa lên table
  user: User;
  admin = ID_ROLE_ADMIN;
  lecture = ID_ROLE_LECTURE;

  dataSelect2: Array<Select2OptionData>;
  option: Options;
  dataConvert: DataConvertSelect2;

  @ViewChild(DataTableDirective, {static: false}) // khai bao cac tuy chon cua dataTable
  dtElement: DataTableDirective;
  dataTableOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  bsModalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private roleService: RoleService,
    private userService: UserService,
    private authention: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.user = this.authention.getCurrentUser;
    this.dataTableOptions = {
      pagingType: 'full_numbers'
    };
    this.setDataSelectRole();
    this.getUsers();
  }

  // lấy danh sách user từ serve đưa lên table
  getUsers() {
    this.users = new Array();
    this.userService.getUsers().subscribe((data: User[]) => {
        data.map(obj => {
          if (obj.roleId == ID_ROLE_LECTURE) {
            if (obj.roleName == 'Lecture') {
              this.users.push(obj);
            }
          }
          if (obj.roleId == ID_ROLE_STUDENT) {
            if (obj.roleName == 'Student') {
              this.users.push(obj);
            }
          }
        });
        this.dataForTable = this.users;
        this.dtTrigger.next();
      },
      error1 => {
        alert(DES_LOAD_DATA_FORM_SERVER);
      }
    );

  }


  openModalDetail(event: Event) {
    const id = (event.target as Element).id;
    const idRole = (event.target as Element).getAttribute('name');
    console.log(idRole);
    const initialState = {
      idRole,
      idUser: id,
    };
    this.bsModalRef = this.modalService.show(DetailUserComponent, {initialState});
    this.bsModalRef.content.onClose.subscribe(result => { // component cha tiếp tục lắng nghe sự kiện từ component con, nếu thực hiện crud sẽ truyền về 1 v và thực hiện reload
      if (result) {
        console.log('ok');
      } else if (!result) {
        alert(DES_LOAD_DATA_FORM_SERVER);
      }
    });
  }

  openMoadlDelete(event: Event) {
    let user: User;
    let id = (event.target as Element).getAttribute('name');
    this.users.map(obj => {
      if (obj.id == id) {
        user = obj;
      }
    });
    const initialState = {
      user: user,
    };
    this.bsModalRef = this.modalService.show(DeleteUserComponent, {initialState});
    this.bsModalRef.content.onClose.subscribe(status => {
      if (status == SUCCESS) {
        this.reload();
      } else if (status == ERROR_INSERT) {
        alert('Người dùng hiện đã có dữ liệu liên quan lên không thể thực hiện xóa!');
      }
    });
  }
  openModalEdit(event: Event) {
    let user: any;
    let id = (event.target as Element).getAttribute('name');
    this.users.map(obj => {
      if (obj.id == id) {
        user = obj;
      }
    });
    const initialState = {
      user: user,
    };
    this.bsModalRef = this.modalService.show(EditUserComponent, {initialState});
    this.bsModalRef.content.onClose.subscribe(status => {
      if (status == SUCCESS) {
        console.log('ok');
        this.reload();
      } else if (status == ERROR_LOAD_DATA) {
        alert(DES_LOAD_DATA_FORM_SERVER);
      } else if (status == ERROR_INSERT) {
        alert('Lỗi khi thêm dữ liệu vào server, đề nghị kiểm tra lại!');
      }
    });
  }

  openModalAdd(event: Event) {
    let idRole = (event.target as Element).getAttribute('name');
    const initialState = {
      roleId: idRole,
    };
    this.bsModalRef = this.modalService.show(AddUserComponent, {initialState});
    this.bsModalRef.content.onClose.subscribe(result => {
      if (result == SUCCESS) {
        this.reload();
      } else if (result == ERROR_INSERT) {
        alert('Lỗi khi thêm dữ liệu vào serve, hoặc do người dùng đã tồn tại, đề nghị thực hiện lại!');
      }
    });
  }

  // khi click vào các vai trò khác nhua sẽ show lên danh sách user thep vao trò đó
  onChangeSelect2(id: any) {
    this.dataForTable = [];
    if (id != undefined) {
      if (id == 'all') {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dataForTable = this.users;
          this.dtTrigger.next();
        });
      } else {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.users.map(user => {
            if (user.roleId == id) {
              this.dataForTable.push(user);
            }
          });
          this.dtTrigger.next();
        });
      }
    }
  }

  setDataSelectRole() {
    this.option = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Tất cả',
    };
    const dataAdapterArray = [];
    this.roleService.getRoles().subscribe((data: any) => {
        this.roles = data;
        this.roles.map(role => {
            if (role.roleName != 'Admin') {
              this.dataConvert = new DataConvertSelect2();
              this.dataConvert.id = role.id;
              if (this.dataConvert.id == ID_ROLE_LECTURE) {
                this.dataConvert.text = 'Giảng viên';
              } else {
                this.dataConvert.text = 'Sinh viên';
              }
              dataAdapterArray.push(this.dataConvert);
            }
          }
          // @ts-ignore
        );
        dataAdapterArray.push(new DataConvertSelect2('all', 'Tất cả'));
        this.dataSelect2 = dataAdapterArray;
      },
      error1 => {
        alert(DES_LOAD_DATA_FORM_SERVER);
      }
    );
  }

  private reload() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.getUsers();
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
