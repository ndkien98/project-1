import { Component, OnInit } from '@angular/core';
import {Subject} from "rxjs";
import {UserService} from "../../../../shared/_service/user.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {User} from "../../../../shared/_models/user";
import {DES_LOAD_DATA_FORM_SERVER, ERROR_INSERT, reload, SUCCESS} from "../../../../shared/_models/constant";
import {BaseService} from "../../../../shared/_service/base.service";

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  public onClose: Subject<boolean>;
  user: User;
  constructor(
    private userService: UserService,
    public bsModalRef: BsModalRef,
  ) { }

  ngOnInit(): void {
    BaseService.checkNullOrUndefine(this.user);
    this.onClose = new Subject();
  }

  deleteUser() {
      this.userService.deleteUser(this.user.id).subscribe((data: any) => {
          // @ts-ignore
          this.onClose.next(SUCCESS);
          this.bsModalRef.hide();
        },
        error => {
          // @ts-ignore
          this.onClose.next(ERROR_INSERT);
          this.bsModalRef.hide();
        },
      );
  }
}
