import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {StatisticalComponent} from './statistical.component';

import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {NgSelect2Module} from 'ng-select2';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    StatisticalComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    NgSelect2Module],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [StatisticalComponent]
})

export class StatisticalModule {

}
