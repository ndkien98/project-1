import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Options} from 'select2';
import {
  baseUrl,
  DataConvertSelect2,
  DES_LOAD_DATA_FORM_SERVER,
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT, PROJECT_DISABLE
} from '../../../../shared/_models/constant';
import {YearsSemesterService} from '../../../../shared/_service/years-semester.service';
import {Select2OptionData} from 'ng-select2';
import {YearsSemester} from '../../../../shared/_models/years-semester';
import {CoursesService} from '../../../../shared/_service/courses.service';
import {Courses} from '../../../../shared/_models/courses';
import {CategoriesService} from '../../../../shared/_service/categories.service';
import {Categories} from '../../../../shared/_models/categories';
import {UserService} from '../../../../shared/_service/user.service';
import {Lecturer, Student} from '../../../../shared/_models/user';
import 'material-icons/iconfont/material-icons.scss';
import {ProjectService} from '../../../../shared/_service/proejct.service';
import {Projects} from "../../../../shared/_models/projects";
import {AuthenticationService} from "../../../../shared/_service/authentication.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  currentFileP: File;
  semesterYearInfor: string;
  coursesInfor: string;
  listOfCourses: Courses[];
  dataArrayAdapter = [];
  nameOfSubject: string;
  lecturerOfCourses: string;
  students: Student[];
  student: Student;
  listofMember: Array<any> = [];
  member: any = {};
  shortDes: any;
  linkDemo: any;
  lectrures;

  html: any; // data of summernote
  config: any = {
    airMode: false,
    tabDisable: true,
    popover: {
      table: [
        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
      ],
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']],
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [
        [
          'font',
          [
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'superscript',
            'subscript',
            'clear',
          ],
        ],
      ],
    },
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      [
        'font',
        [
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'superscript',
          'subscript',
          'clear',
        ],
      ],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']],
    ], codeviewFilter: true, codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true,
  };

  inforProjectForm: FormGroup;
  option: Options;
  optionForLead: Options;
  dataConvert: DataConvertSelect2;
  dataForSelect2YearSemesters: Array<Select2OptionData>;
  dataForSelect2Courses: Array<Select2OptionData>;
  dataForSelect2Category: Array<Select2OptionData>;
  dataForselect2LeadProject: Array<Select2OptionData>;

  constructor(
    private formBuider: FormBuilder,
    private yearsSemesterService: YearsSemesterService,
    private coursesService: CoursesService,
    private categoryService: CategoriesService,
    private userService: UserService,
    private projectService: ProjectService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.students = new Array();
    this.listOfCourses = new Array();
    this.lectrures = new Array();
    this.createForm();
    this.setAllLeadProjectsForSelect2();
    this.setAllYearsSemesterForSelect2();
    this.setAllCoursesForSelec2();
    this.setAllCategoryForSelect2();
  }

  createForm() {
    this.inforProjectForm = this.formBuider.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      idCourses: ['', Validators.required],
      codeCategory: ['', Validators.required],
    });
  }

  setAllYearsSemesterForSelect2() {
    this.option = {
      theme: 'classic',
      width: '100%',
    };
    // tslint:disable-next-line:prefer-const
    this.yearsSemesterService.getAllYearsSemester().subscribe((data: YearsSemester[]) => {
        data.map(yearsSemes => {
          this.semesterYearInfor = 'Học kỳ ' + yearsSemes.semester + ' - Năm học ' + yearsSemes.year + ' - ' + ++yearsSemes.year;
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = yearsSemes.id;
          this.dataConvert.text = this.semesterYearInfor;
          this.dataArrayAdapter.push(this.dataConvert);
        });
        this.dataForSelect2YearSemesters = this.dataArrayAdapter;
        this.dataArrayAdapter = [];
      },
      error1 => {
        alert('Lỗi tải data từ serve, đề nghị tải lại trang');
      }
    );
  }

  setAllCoursesForSelec2() {
    this.nameOfSubject = 'Chọn lớp học phần';
    this.lecturerOfCourses = 'Mã giảng viên phụ trách';
    let dataAdapter = new Array();
    this.coursesService.getAllCourses().subscribe((data: Courses[]) => {
      // @ts-ignore
      this.listOfCourses = data;
      data.map(courses => {
        this.coursesInfor = 'Mã MH: ' + courses.subjectCode + ' - Nhóm MH: ' + courses.subjectGroup;
        this.dataConvert = new DataConvertSelect2();
        this.dataConvert.id = courses.id;
        this.dataConvert.text = this.coursesInfor;
        dataAdapter.push(this.dataConvert);
      });
      // @ts-ignore
      this.dataForSelect2Courses = dataAdapter;
      // @ts-ignore
      this.dataArrayAdapter = [];
    });
  }

  setAllCategoryForSelect2() {
    this.categoryService.getAllCategories().subscribe((data: Categories[]) => {
      data.map(category => {
        this.dataConvert = new DataConvertSelect2();
        this.dataConvert.id = category.categoryCode;
        this.dataConvert.text = category.categoryName;
        this.dataArrayAdapter.push(this.dataConvert);
      });
      this.dataForSelect2Category = this.dataArrayAdapter;
      this.dataArrayAdapter = [];
    });
  }
  setAllLeadProjectsForSelect2() {
    this.optionForLead = {
      width: '100%',
    };
    this.student = new Student();
    this.student.fullName = 'Tên nhóm trưởng';
    this.student.classCode = 'Mã Lớp';
    this.http.get(baseUrl + 'api/users/get-students').subscribe((data: Student[]) => {
      data.map(student => {
        if ((student.roleName == 'Student' && student.roleId == ID_ROLE_STUDENT)) {
          this.students.push(student);
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = student.id;
          this.dataConvert.text = student.username;
          this.dataArrayAdapter.push(this.dataConvert);
        }
      });
      this.dataForselect2LeadProject = this.dataArrayAdapter;
      this.dataArrayAdapter = [];
    });
    this.userService.getLectures().subscribe((data: Lecturer[]) => {
      data.map(obj => {
        if (obj.roleName == 'Lecture' && obj.roleId == ID_ROLE_LECTURE) {
         this.lectrures.push(obj);
        }
      });
    });
  }

  // lay lop hoc phan theo nam hoc, hoc ky
  onChanYearSemester(id: any) {
    this.listOfCourses.map(courses => {
      if (courses.yearSemesterId == id) {
        this.coursesInfor = 'Mã MH: ' + courses.subjectCode + ' - Nhóm MH: ' + courses.subjectGroup;
        this.dataConvert = new DataConvertSelect2();
        this.dataConvert.id = courses.id;
        this.dataConvert.text = this.coursesInfor;
        this.dataArrayAdapter.push(this.dataConvert);
      }
    });
    this.dataForSelect2Courses = this.dataArrayAdapter;
    this.dataArrayAdapter = [];
  }

  public onChangeCourses(id: any) {
    this.listOfCourses.map(courses => {
      if (courses.id == id) {
        // @ts-ignore
        this.nameOfSubject = courses.subjectName + ' - Mã Lớp: ' + courses.classCode;
        // @ts-ignore
        this.lecturerOfCourses = courses.lecturerCode;
      }
    });
  }
  public onChangeLead(id: any) {
    this.students.map(student => {
      if (id == student.id) {
        this.student = student;
        return;
      }
    });
  }

  submitFileP(event) {
    this.currentFileP = event.target.files[0];
    const preview = document.getElementById('viewAvatarP');
    const render = new FileReader();
    render.onload = function(e) {
      // @ts-ignore
      preview.setAttribute('src', e.target.result);
    };
    render.readAsDataURL(this.currentFileP);
  }
  addMember() {
    this.listofMember.push(this.member)
    this.member = {};
  }

  deleteMember(index) {
    this.listofMember.splice(index, 1);
  }

  test() {
    console.log('Nhóm trưởng' + this.student.fullName);
    console.log(this.listofMember);
  }

  saveProject() {
    if (this.inforProjectForm.status == 'INVALID'){
      alert('Thông tin cơ bản của đồ án chưa đầy đủ, Đề nghị điền thông tin đầy đủ!');
      return;
    }
    if (!this.currentFileP) {
      alert('Đồ án chưa có ảnh đại diện, đề nghị chọn ảnh đại diện!');
      return;
    }
    if (!this.shortDes) {
      alert('Đồ án chưa có mô tả tóm tắt, đề nghị điền mô tả tóm tắt!');
      return;
    }
    if (this.student.fullName == 'Tên nhóm trưởng') {
      alert('Đồ án chưa có nhóm trưởng, đề nghị chọn nhóm trưởng!');
      return;
    }
    if (!this.html) {
      alert('Đồ án chưa có mô tả chi tiết, Đề nghị điền mô tả chi tiết cho đồ án! ');
      return;
    }
    if (!this.linkDemo) {
      alert('Đồ án chưa có link chạy thử, Đề nghị điền thông tin link chạy thử! ');
      return;
    }
    let project = new Projects();

    project.projectCode = this.inforProjectForm.controls.code.value;
    project.projectName = this.inforProjectForm.controls.name.value;
    project.courseId = this.inforProjectForm.controls.idCourses.value;
    project.categoryCode = this.inforProjectForm.controls.codeCategory.value;
    project.shortDescription = this.shortDes;
    project.studentCode = this.student.username;
    project.projectMembers = this.listofMember;
    project.detailedDescription = this.html;
    project.demoLink = this.linkDemo;
    project.status = PROJECT_DISABLE;
    project.createdBy = this.authenticationService.getCurrentUser.username;
    this.projectService.addProject(project).subscribe((data: Projects) => {
      this.projectService.addAvatar(this.currentFileP, data.id).subscribe((data2: Projects) => {
        alert('Thêm đồ án thành công, Đồ án đang trong danh sách chờ sét duyệt!');
        this.router.navigateByUrl('/management/project');
      },
        error1 => alert('Lỗi khi thêm ảnh của đồ án!'));
        this.router.navigateByUrl('/management/project');
    },
      error1 => alert('Lỗi khi thực hiện thêm đò án, Đề nghị kiểm tra lại thông tin!')
    );
  }
  cancel() {
    this.router.navigateByUrl('/management/project');
  }
}

