import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'; // la bien router dang singer la ton tai duy nhat
import { Location } from '@angular/common';
import {Projects} from '../../../../shared/_models/projects';
import {ProjectService} from '../../../../shared/_service/proejct.service';
import {Student} from '../../../../shared/_models/user';
import {UserService} from '../../../../shared/_service/user.service';
import {PROJECT_DISABLE, PROJECT_ENABLE} from "../../../../shared/_models/constant";
import {DISMISS_REASONS} from "ngx-bootstrap/modal/modal-options.class";

@Component({
  selector: 'app-detail-project',
  templateUrl: './detail-project.component.html',
  styleUrls: ['./detail-project.component.css']
})
export class DetailProjectComponent implements OnInit {
  project: Projects;
  student: Student;
  students: any[];
  enable = PROJECT_ENABLE;
  disable = PROJECT_DISABLE;

  config: any = {
    airMode: false,
    tabDisable: true,
    popover: {
      table: [
        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
      ],
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']],
      ],
      link: [['link', ['linkDialogShow', 'unlink']]],
      air: [
        [
          'font',
          [
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'superscript',
            'subscript',
            'clear',
          ],
        ],
      ],
    },
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      [
        'font',
        [
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'superscript',
          'subscript',
          'clear',
        ],
      ],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']],], codeviewFilter: true, codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true,
  };

  constructor(
    private router1: Router,
    private router: ActivatedRoute,
    private location: Location,
    private projectService: ProjectService,
    private userservice: UserService

  ) { }

  ngOnInit(): void {
    this.student = new Student();
    this.project = new Projects();
    this.students = new Array();
    this.getProjectFromRouter();
  }

  getProjectFromRouter(): void {
    this.userservice.getStudents().subscribe((list: Student[]) => {
      this.students = list;
    });
    const id = +this.router.snapshot.paramMap.get('id'); // lay ra duoc id ma param truyen ve , "+" de bien string->number
    this.projectService.getProjectById(id).subscribe((data: any) => {
      this.project = data;
      console.log(this.project);
      this.students.map(obj => {
        if (obj.username == this.project.studentCode) {
          this.student = obj;
        }
      });
    },
      error1 => {
      alert('Lỗi tải data từ server, đề nghị tair lại trang!');
      this.goBack();
      }
    );
  }

  save(): void {
    this.goBack();
  }
  goBack(): void {
    // this.location.back();
    if (this.project.status == PROJECT_DISABLE) {
      // @ts-ignore
      this.router1.navigateByUrl('/management/revise');
    } else {
      // @ts-ignore
      this.router1.navigateByUrl('/management/project');
    }
  }
  deleteProject() {
    this.projectService.deleteProject(this.project.id).subscribe((data: any) => {
      alert('Xóa đồ án thành công!');
      this.goBack();
    }, error1 => {
      alert('Xóa đồ án thất bai!');
      this.goBack();
    }
    );
  }
  editProject() {
    this.router1.navigateByUrl('/management/project/edit/' + this.project.id);
  }
}
