import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {Select2OptionData} from 'ng-select2';
import {
  DataConvertSelect2,
  ID_ROLE_ADMIN,
  ID_ROLE_LECTURE,
  PROJECT_DISABLE,
  PROJECT_ENABLE
} from '../../../shared/_models/constant';
import {ProjectService} from '../../../shared/_service/proejct.service';
import {YearsSemesterService} from '../../../shared/_service/years-semester.service';
import {Router} from '@angular/router';
import {YearsSemester} from '../../../shared/_models/years-semester';
import {Options} from 'select2';
import {AuthenticationService} from "../../../shared/_service/authentication.service";
import {User} from "../../../shared/_models/user";
import {Projects} from "../../../shared/_models/projects";

@Component({
  selector: 'app-revise',
  templateUrl: './revise.component.html',
  styleUrls: ['./revise.component.css']
})
export class ReviseComponent implements OnInit {
  projects;
  semesterYear: string;
  projectsOfLecture;
  user: User;
  admin = ID_ROLE_ADMIN;
  lecture = ID_ROLE_LECTURE;

  @ViewChild(DataTableDirective, {static: false}) // khai bao cac tuy chon cua dataTable
  dtElement: DataTableDirective;
  dataTableOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  dataSelect2: Array<Select2OptionData>;
  option: Options;
  dataConvert: DataConvertSelect2;
  selected: boolean;


  constructor(
    private projectService: ProjectService,
    private yearsSemesterService: YearsSemesterService,
    private router: Router,
    private authentication: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.projects = new Array();
    this.projectsOfLecture = new Array();
    this.user = this.authentication.getCurrentUser;
    this.loadAllProject();
    this.setAllYearsSemesterForSelect2();
  }

  private loadAllProject() {
    this.dataTableOptions = {
      pagingType: 'full_numbers'
    };
    return this.projectService.getAllProject().subscribe(
      (data: Projects[]) => {
        data.map(project => {
          if (project.status == PROJECT_DISABLE) {
            this.projects.push(project);
          }
          if (project.status == PROJECT_DISABLE && project.lecturerCode == this.user.username) {
            this.projectsOfLecture.push(project);
          }
        });
        this.dtTrigger.next();
      },
      error1 => alert('Lỗi tải data từ server, đề nghị tại lại trang')
    );
  }

  setAllYearsSemesterForSelect2() {
    this.option = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Chọn học kỳ - năm học',
    };
    const dataArray = [];
    // tslint:disable-next-line:prefer-const
    this.yearsSemesterService.getAllYearsSemester().subscribe((data: YearsSemester[]) => {
        data.map(yearsSemes => {
          this.semesterYear = 'Học kỳ ' + yearsSemes.semester + ' - Năm học ' + yearsSemes.year + ' - ' + ++yearsSemes.year;
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = yearsSemes.id;
          this.dataConvert.text = this.semesterYear;
          dataArray.push(this.dataConvert);
        });
        dataArray.push(new DataConvertSelect2('all', 'Tất cả năm học học kỳ'));
        this.dataSelect2 = dataArray;
      },
      error1 => {
        alert('Lỗi tải data từ serve, đề nghị tải lại trang');
      }
    );
  }

  public onChange(id: any) {
    if (id !== undefined) {
      if (id == 'all') {
        this.projects = [];
        this.projectService.getAllProject().subscribe((data: any) => {
          data.map(project => {
            if (project.status == PROJECT_DISABLE) {
              this.projects.push(project);
            }
          });
        });
      } else {
        this.projects = [];
        this.projectService.getProjectByYearSemesterId(id).subscribe((data: any) => {
          // @ts-ignore
          console.log(data);
          data.map(project => {
            if (project.status == PROJECT_DISABLE) {
              this.projects.push(project);
            }
          });
        });
      }
    }
  }
  public reviseProject(event: Event) {
    const id = (event.target as Element).getAttribute('name');
    this.projects.map(obj => {
      if (obj.id == id) {
        obj.status = PROJECT_ENABLE;
        this.projectService.editProject(obj, obj.id).subscribe((data: any) => {
          this.projectService.getAllProject().subscribe(( response: any) => {
            this.projects = [];
            response.map(obj => {
              if (obj.status == PROJECT_DISABLE) {
                this.projects.push(obj);
              }
            });
            alert('Duyệt đồ án thành công!');
          });
        },
          error1 => {alert('Duyệt đồ án không thành công!'); }
        );
      }
    });
    // this.router.navigateByUrl('/management/project/add');
  }
}
