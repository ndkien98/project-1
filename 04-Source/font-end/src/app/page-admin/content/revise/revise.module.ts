import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';
import {ReviseComponent} from './revise.component';
import {RouterModule} from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelect2Module} from 'ng-select2';

@NgModule({
  declarations: [
    ReviseComponent,
  ],

  imports: [
    CommonModule,
    DataTablesModule,
    NgSelect2Module,
    RouterModule,
    FormsModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: []
})

export class ReviseModule {

}
