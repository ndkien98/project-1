import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {reload} from "../../../../shared/_models/constant";
import {Categories} from "../../../../shared/_models/categories";
import {CategoriesService} from "../../../../shared/_service/categories.service";
import {BaseService} from "../../../../shared/_service/base.service";

@Component({
  selector: 'app-delete-category',
  templateUrl: './delete-category.component.html',
  styleUrls: ['./delete-category.component.css']
})
export class DeleteCategoryComponent implements OnInit {
  categories: Categories;
  public onClose: Subject<boolean>;

  constructor(
    public categoriesService: CategoriesService,
    public bsModalRef: BsModalRef,
  ) {
  }

  ngOnInit(): void {
    BaseService.checkNullOrUndefine(this.categories);
    this.onClose = new Subject();
  }
  delete() {
    this.categoriesService.deleteCategories(this.categories.id).subscribe((data: boolean) => {
        this.onClose.next(reload);
        this.bsModalRef.hide();
      },
      error1 => {
        this.onClose.next(!reload);
        this.bsModalRef.hide();
      }
    );
  }
}
