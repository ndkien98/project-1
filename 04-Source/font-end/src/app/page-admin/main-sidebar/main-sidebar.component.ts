import { Component, OnInit } from '@angular/core';
import * as Inputmask from 'inputmask';
import * as $ from 'jquery';
import {AuthenticationService} from '../../shared/_service/authentication.service';
import {ProjectService} from "../../shared/_service/proejct.service";
import {ObjectCustom} from "../../shared/_models/user";
import {ID_ROLE_ADMIN, ID_ROLE_LECTURE, ID_ROLE_STUDENT} from "../../shared/_models/constant";

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html'
})
export class MainSidebarComponent implements OnInit {
  constructor(
    private authentication: AuthenticationService,
  ) { }
  user: ObjectCustom;
  avatar: any;
  admin;
  stuent;
  lecture;
  check = true;

  ngOnInit(): void {
    this.admin = ID_ROLE_ADMIN;
    this.lecture = ID_ROLE_LECTURE;
    this.stuent = ID_ROLE_STUDENT;
    this.user = new ObjectCustom();
    this.user = this.authentication.getCurrentUser;
    if (!this.user.userAvatarUrl) {
      this.avatar = 'assets/admin-dashboard/dist/img/user2-160x160.jpg';
    } else {
      this.avatar = this.user.userAvatarUrl;
    }
    Inputmask().mask(document.querySelectorAll('input'));
  }

}
