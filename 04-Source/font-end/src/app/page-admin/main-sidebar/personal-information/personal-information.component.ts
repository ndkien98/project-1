import { Component, OnInit } from '@angular/core';
import {Options} from 'select2';
import {AuthenticationService} from "../../../shared/_service/authentication.service";
import {
  DataConvertSelect2,
  ERROR_INSERT,
  ERROR_LOAD_DATA,
  ID_ROLE_LECTURE,
  ID_ROLE_STUDENT, SUCCESS
} from "../../../shared/_models/constant";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Lecturer, ObjectCustom, Student, User} from "../../../shared/_models/user";
import {Select2OptionData} from "ng-select2";
import {UserService} from "../../../shared/_service/user.service";
import {BsModalRef} from "ngx-bootstrap/modal";
import {DepartmentService} from "../../../shared/_service/department.service";
import {emailValidator} from "../../../shared/_models/custom-validators";
import {BaseService} from "../../../shared/_service/base.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit {
  userFormGroup: FormGroup;
  LECTURE = ID_ROLE_LECTURE;
  STUDENT = ID_ROLE_STUDENT;
  currentFile: File;
  avartar: any;
  gender: any;
  user: ObjectCustom;

  public select2Data: Array<Select2OptionData>; // data chính đổ vào select 2
  public options: Options;                        // option của select 2
  private dataConvert: DataConvertSelect2;
  constructor(
    private authentication: AuthenticationService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private departmentService: DepartmentService,
    private router: Router
  ) {
  }
  ngOnInit(): void {
    this.user = new ObjectCustom();
    this.user = this.authentication.getCurrentUser;
    this.createForm();
    this.setdata();
    this.setDataForSelect2();
  }
  createForm() {
    if (this.user.userAvatarUrl == null) {
      this.avartar = 'assets/admin-dashboard/dist/img/user2-160x160.jpg';
    } else {
      this.avartar = this.user.userAvatarUrl;
    }
    this.userFormGroup = this.formBuilder.group({
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      birthDate: ['', Validators.required],
      emailAddress: ['', [Validators.required, emailValidator()]],
      phoneNumber: ['', Validators.required],
      roleName: [''],
      address: ['', Validators.required],
    });
  }
  setdata() {
    try {
      if (this.user.roleId == this.LECTURE) {
        this.userService.getLectureById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
                username: this.user.username,
                fullname: this.user.fullName,
                birthDate: this.user.birthDate,
                emailAddress: this.user.emailAddress,
                phoneNumber: this.user.phoneNumber,
                roleName: this.user.roleName,
                address: this.user.departmentCode
              }
            );
          },
          error1 => {
          BaseService.checkNullOrUndefine(null);
          }
        );
      } else if (this.user.roleId == this.STUDENT) {
        this.userService.getStudentById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
              username: this.user.username,
              fullname: this.user.fullName,
              birthDate: this.user.birthDate,
              emailAddress: this.user.emailAddress,
              phoneNumber: this.user.phoneNumber,
              roleName: this.user.roleName,
              address: this.user.classCode
            });
          },
          error1 => {
            BaseService.checkNullOrUndefine(null);
          }
        );
      } else {
        this.userService.getUserById(this.user.id).subscribe((data: any) => {
            this.user = data;
            this.userFormGroup.patchValue({
              username: this.user.username,
              fullname: this.user.fullName,
              birthDate: this.user.birthDate,
              emailAddress: this.user.emailAddress,
              phoneNumber: this.user.phoneNumber,
              roleName: this.user.roleName,
              address: 'address'
            });
          },
          error1 => {
            BaseService.checkNullOrUndefine(null);
          });
      }
    } catch (exeption) {
      BaseService.checkNullOrUndefine(null);
    }
    BaseService.checkNullOrUndefine(this.user);
    this.gender = this.user.gender;
  }

  private setDataForSelect2() {
    this.options = {
      theme: 'classic',
      width: '100%',
      placeholder: 'Chọn tên bộ môn',
      dropdownAutoWidth: true,
    };
    const dataArr = [];
    this.departmentService.getAllDepartment().subscribe((data: any) => {
      data.map(obj => {
          this.dataConvert = new DataConvertSelect2();
          this.dataConvert.id = obj.departmentCode;
          this.dataConvert.text = obj.departmentName;
          dataArr.push(this.dataConvert);
        }, error => {
          BaseService.checkNullOrUndefine(null);
        }
      );
      this.select2Data = dataArr;
    });
  }
  submitFile(event) {
    this.currentFile = event.target.files[0];
    const preview = document.getElementById('viewAvatar');
    const render = new FileReader();
    render.onload = function(e) {
      // @ts-ignore
      preview.setAttribute('src', e.target.result);
    };
    render.readAsDataURL(this.currentFile);
  }

  onChangeGender(event: Event) {
    this.gender = (event.target as Element).getAttribute('value');
  }

  onSubmit() {
    console.log(this.userFormGroup.value);
    if (this.user.roleId.toString() == ID_ROLE_LECTURE.toString()) {
      // @ts-ignore
      const lecturer = new Lecturer();
      lecturer.id = this.user.id;
      lecturer.roleId = this.user.roleId;
      lecturer.username = this.userFormGroup.controls.username.value;
      lecturer.fullName = this.userFormGroup.controls.fullname.value;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.gender = this.gender;
      lecturer.birthDate = this.userFormGroup.controls.birthDate.value;
      lecturer.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      lecturer.departmentCode = this.userFormGroup.controls.address.value;
      lecturer.emailAddress = this.userFormGroup.controls.emailAddress.value;
      lecturer.status = 1;
      this.userService.editLecture(lecturer, lecturer.id).subscribe((data: any) => {
          this.editAvatar();
          this.editSuccess();
        }, error1 => {
          this.editError();
        }
      );
    } else if (this.user.roleId.toString() == ID_ROLE_STUDENT) {
      // @ts-ignore
      const student = new Student();
      student.id = this.user.id;
      student.roleId = this.user.roleId;
      student.username = this.userFormGroup.controls.username.value;
      student.fullName = this.userFormGroup.controls.fullname.value;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.gender = this.gender;
      student.birthDate = this.userFormGroup.controls.birthDate.value;
      student.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      student.classCode = this.userFormGroup.controls.address.value;
      student.emailAddress = this.userFormGroup.controls.emailAddress.value;
      student.status = 1;
      console.log(student);
      this.userService.editStudent(student, student.id).subscribe((data: any) => {
          this.editAvatar();
          this.editSuccess();
        }, error1 => {
          this.editError();
        }
      );
    } else if (this.user.roleId == 1) {
      const user = new User();
      user.id = this.user.id;
      user.roleId = this.user.roleId;
      user.username = this.userFormGroup.controls.username.value;
      user.fullName = this.userFormGroup.controls.fullname.value;
      user.birthDate = this.userFormGroup.controls.birthDate.value;
      user.gender = this.gender;
      user.birthDate = this.userFormGroup.controls.birthDate.value;
      user.phoneNumber = this.userFormGroup.controls.phoneNumber.value;
      user.emailAddress = this.userFormGroup.controls.emailAddress.value;
      user.status = 1;
      console.log(user);
      this.userService.editUser(user, user.id).subscribe((data: any) => {
          this.editAvatar();
          this.editSuccess();
        }, error1 => {
          this.editError();
        }
      );
    } else {
      BaseService.checkNullOrUndefine(null);
    }
  }
  private editAvatar() {
    if (!this.currentFile) {
      return;
    }
    this.userService.editAvartar(this.currentFile,this.user.id).subscribe((data: User) => {
        console.log(data);
      },
      error1 => {
        alert('edit avatar error');
      }
    );
  }
  private editSuccess() {
    alert('Chỉnh sửa thành công!');
    this.router.navigateByUrl('/management/user');
  }
  private editError() {
    alert('Chỉnh sửa thất bại!');
  }
}
