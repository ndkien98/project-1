import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from '../../shared/_service/authentication.service';
import {User} from "../../shared/_models/user";
import {UserService} from "../../shared/_service/user.service";
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  user: any;
  avatar: any;
  constructor(
    private router: Router,
    private authentication: AuthenticationService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.user = this.authentication.getCurrentUser;
    console.log(this.user);
    if (this.user == null) {
      this.authentication.logout();
    }
    if (!this.user.userAvatarUrl) {
      this.avatar = 'assets/admin-dashboard/dist/img/user2-160x160.jpg';
    } else {
      this.avatar = this.user.userAvatarUrl;
    }
  }

  click() {
    this.router.navigate(['/login']);
    this.authentication.logout();
  }
}
