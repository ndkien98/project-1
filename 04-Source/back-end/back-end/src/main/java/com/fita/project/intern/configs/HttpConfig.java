package com.fita.project.intern.configs;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class HttpConfig {
    public HttpHeaders headers;
    public HttpHeaders headerDatalake;

    public static HttpConfig instance;

    public static HttpConfig getInstance() {
        if (instance == null){
            instance = new HttpConfig();
            instance.setHttpHeader();
            instance.setHttpHeaderDatalake();
        }

        return instance;
    }

    private HttpConfig() {
    }

    private void setHttpHeader() {
        this.headers = new HttpHeaders();
        this.headers.add("Content-Type", "application/json");
        this.headers.add("Authorization", "M87xs3IDAtNSXSISN1LXj7MC0lFNTXHq");
    }

    private void setHttpHeaderDatalake() {
        this.headerDatalake = new HttpHeaders();
        this.headerDatalake.add("Content-Type", "application/json");
        this.headerDatalake.add("Authorization", "M87xs3IDAtNSXSISN1LXj7MC0lFNTXHq");
    }
}
