package com.fita.project.intern.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {
    public static boolean isExist(String absPath) {
        return Files.exists(Paths.get(absPath));
    }

    public static String getFileNameFromUrl(String url) {
        try {
            URL u = new URL(url);
            String path = u.getPath();
            int idx = path.lastIndexOf('/');
            if (idx == -1 || idx == path.length() - 1) {
                return "";
            }

            return path.substring(idx + 1);
        } catch (MalformedURLException e) {
            return "";
        }
    }

    public static String getExtension(String fileName) {
        int idx = fileName.lastIndexOf('.');
        if (idx == -1 || idx == fileName.length() - 1) {
            return "";
        }

        return fileName.substring(idx + 1);
    }
}
